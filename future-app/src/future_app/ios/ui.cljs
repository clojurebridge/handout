(ns future-app.ios.ui
  (:require [reagent.core :as r :refer [atom]]))

  (def ReactNative (js/require "react-native"))

  (def text (r/adapt-react-class (.-Text ReactNative)))
  (def view (r/adapt-react-class (.-View ReactNative)))

  (defn Header [info]
    [view {:style {:flex-direction "row" :margin 5 :align-items "stretch" :height 55 :background-color "indigo"}}
      [text {:style {:font-size 30 :font-weight "100" :margin-bottom 20 :text-align "left"}} (:name @info)]])

  (defn Layout [info]
      [view {:style {:flex-direction "column" :margin-top 20 :align-items "stretch"}}
      [Header info]])
