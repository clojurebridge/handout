(ns future-app.ios.core
  (:require [reagent.core :as r :refer [atom]]
            [future-app.ios.ui :refer [Layout]]
            [future-app.ios.db :refer [info]]))

(def ReactNative (js/require "react-native"))

(def app-registry (.-AppRegistry ReactNative))
(def text (r/adapt-react-class (.-Text ReactNative)))
(def view (r/adapt-react-class (.-View ReactNative)))

(defn app-root []
     [Layout info])

(defn init []
      (.registerComponent app-registry "FutureApp" #(r/reactify-component app-root)))
